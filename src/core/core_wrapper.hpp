#pragma once

#include <QObject>

class CoreWrapper : public QObject
{
    Q_OBJECT
public:
    explicit CoreWrapper(QObject *parent = nullptr);
    Q_INVOKABLE QString getFooValue();
};
